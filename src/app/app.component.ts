import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `<h1>Hello {{name}}</h1>`,
})

@Component({
  selector: 'myApp',
  template: `<div ng-app="myApp">
<div ng-controller="myCtrl">
<nvd3 options="options" data="data"></nvd3>
</div>
</div>`,
})
export class AppComponent  { name = 'Angular'; }
